import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {CellEnum} from '../cell/cellEnum';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  // logic
  private currentPlayer: CellEnum;
  public board: CellEnum[][];
  // inputs & outputs
  @Output() endGameStatsEvent = new EventEmitter<any>();
  public statusMessage;
  @Input() isGameOver: boolean;

  constructor() {
  }

  ngOnInit() {
    this.newGame();
  }

  get gameOver(): boolean {
    return this.isGameOver;
  }

  /**
   * @description initial creation of the board
   */
  newGame(): void {
    if (!this.isGameOver) {
      this.board = [];
      for (let row = 0; row < 3; row++) {
        this.board[row] = [];
        for (let col = 0; col < 3; col++) {
          this.board[row][col] = CellEnum.EMPTY;
        }
      }
      this.currentPlayer = CellEnum.X;
      this.statusMessage = `Player ${this.currentPlayer}'s turn`;
    }
  }

  restart(): void {
    this.isGameOver = false;
    this.newGame();
  }

  /**
   * @description move condition and logic
   */
  move(row: number, col: number): void {
    // console.log(`row :${row} / column: ${col} `);
    if (!this.isGameOver && this.board[row][col] === CellEnum.EMPTY) {
      this.board[row][col] = this.currentPlayer;
      if (this.isDraw()) {
        this.statusMessage = `It's a Draw!`;
        setTimeout(() => {
          this.newGame();
        }, 1000);
      } else if (this.isWin()) {
        this.statusMessage = `Player ${this.currentPlayer} won!`;
        this.sendEndGameStats(this.currentPlayer);
        setTimeout(() => {
          this.newGame();
        }, 1000);
      } else {
        this.currentPlayer = this.currentPlayer === CellEnum.X ? CellEnum.O : CellEnum.X;
        this.statusMessage = `Player ${this.currentPlayer}'s turn`;
      }
    }
  }

  /**
   * @description draw conditions of the board
   */
  isDraw(): boolean {
    for (const columns of this.board) {
      for (const col of columns) {
        if (col === CellEnum.EMPTY) {
          return false;
        }
      }
    }
    return !this.isWin();
  }

  /**
   * @description win conditions of the board
   */
  isWin(): boolean {
    // horizontal
    for (const columns of this.board) {
      if (columns[0] === columns[1] &&
        columns[0] === columns[2] &&
        columns[0] !== CellEnum.EMPTY
      ) {
        return true;
      }
    }
    // vertical
    for (let col = 0; col < this.board[0].length; col++) {
      if (
        this.board[0][col] === this.board[1][col] &&
        this.board[0][col] === this.board[2][col] &&
        this.board[0][col] !== CellEnum.EMPTY
      ) {
        return true;
      }
    }
    // diagonals
    if (
      this.board[0][0] === this.board[1][1] &&
      this.board[0][0] === this.board[2][2] &&
      this.board[0][0] !== CellEnum.EMPTY
    ) {
      return true;
    }

    if (
      this.board[0][2] === this.board[1][1] &&
      this.board[0][2] === this.board[2][0] &&
      this.board[0][2] !== CellEnum.EMPTY
    ) {
      return true;
    }

    if (
      this.board[2][0] === this.board[1][1] &&
      this.board[2][0] === this.board[2][2] &&
      this.board[2][0] !== CellEnum.EMPTY
    ) {
      return true;
    }
    return false;
  }


  /**
   * @description output event that sends game winner
   */
  sendEndGameStats(gameWinner) {
    this.endGameStatsEvent.emit(gameWinner);
  }
}
