import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {BoardComponent} from './board/board.component';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  // global game
  player1Wins = 0;
  player2Wins = 0;
  winPercentageX = 0;
  winPercentageO = 0;
  gamesPlayed = 0;
  gameHistory = Array(9).fill('');
  globalTime = new Date(0, 0, 0);
  gameTime = new Date(0, 0, 0);
  // current game
  currentGames = 0;
  currentPlayer1Wins = 0;
  currentPlayer2Wins = 0;
  // logic
  gameSubscription: Subscription;
  @ViewChild(BoardComponent, {static: false}) board: BoardComponent;
  isGameOver = false;

  ngOnInit() {
    interval(1000)
      .subscribe(t => {
        this.globalTime = new Date(0, 0, 0);
        this.globalTime.setSeconds(t);
      });
    this.startGameTimer();
  }

  startGameTimer() {
    this.gameSubscription = interval(1000)
      .subscribe(t => {
        this.gameTime = new Date(0, 0, 0);
        this.gameTime.setSeconds(t);
      });
  }

  /**
   * @description get last game winner based on output event from board and do some calculations for statistics
   */
  processLastGameStats(event) {
    this.gamesPlayed = this.gamesPlayed + 1;
    if (event === 'X') {
      this.player1Wins = this.player1Wins + 1;
      this.gameHistory.unshift('P1');
      this.gameHistory.pop();
      this.restartGameTimer();
    } else {
      this.player2Wins = this.player2Wins + 1;
      this.gameHistory.unshift('P2');
      this.gameHistory.pop();
      this.restartGameTimer();
    }
    // percentage of total victories
    this.winPercentageO = Math.floor((this.player2Wins / this.gamesPlayed) * 100);
    this.winPercentageX = Math.floor((this.player1Wins / this.gamesPlayed) * 100);

    this.checkBestOfFive();
  }

  /**
   * @description check if a player as reached 5 wins, if so returns a status message and adds to game history
   */
  checkBestOfFive(): void {
    if (this.player1Wins === 5 || this.player2Wins === 5) {
      this.isGameOver = true;
      this.gameSubscription.unsubscribe();
    }
  }

  /**
   * @description clears current game values
   */
  restartGame(): void {
    this.isGameOver = false;
    this.player1Wins = 0;
    this.player2Wins = 0;
    this.gamesPlayed = 0;
    this.gameHistory = Array(9).fill('');
    this.winPercentageX = 0;
    this.winPercentageO = 0;
    this.restartGameTimer();
    this.board.restart();
  }

  /**
   * @description unsubscribe to restart timer
   */
  restartGameTimer(): void {
    this.gameSubscription.unsubscribe();
    this.startGameTimer();
  }
}
